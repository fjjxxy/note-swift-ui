//
//  WriteNote.swift
//  noteswift
//
//  Created by lzh on 2022/5/31.
//

import SwiftUI
import UIKit

struct WriteNote: View {
    @State var note:NoteModel?=nil
    
    @State var title=""
    @State var content=""
    @State var createTime=Date().milliStamp
    var screenWidth: CGFloat = UIScreen.main.bounds.width
    var screenHeight: CGFloat = UIScreen.main.bounds.height
    @ObservedObject var sqliteViewModel = NoteDBUtils.single
    @State var notes: [NoteModel] = []
    @Environment(\.presentationMode) var presentationMode
    //    init() {
    //
    //        ToastView.appearance().font=UIFont(name: "Arial", size: 18)
    //        ToastView.appearance().bottomOffsetPortrait=screenHeight/2
    //        ToastView.appearance().bottomOffsetLandscape=screenWidth/2
    //
    //    }
    var body: some View {
        
        NavigationView{
            ScrollView{
                VStack (alignment:.leading){
                    Section(header:Text("标题*").padding(.leading).padding(.top).foregroundColor(.red)){
                        HStack {
                            TextField("", text:$title ).frame(height:40).padding(.leading,10).cornerRadius(10).overlay(
                                RoundedRectangle(cornerRadius: 10, style: .continuous)
                                    .stroke(Color.color_gray_border, lineWidth: 1)
                            )
                        }.padding(10)
                    }
                    Section(header:Text("内容*").foregroundColor(.red).padding(.leading), content: { HStack {
                        TextEditor( text:self.$content ).padding(.top).lineLimit(5).frame(height:200).padding(.leading,10).cornerRadius(10).overlay(
                            RoundedRectangle(cornerRadius: 10, style: .continuous)
                                .stroke(Color.color_gray_border, lineWidth: 1)
                        )
                        
                    }.padding(10)})
                    HStack{
                        Spacer()
                        Button(action: {
                            if (self.note?.id==nil) {
                                insert()
                            }else{
                            
                                note?.content=self.content
                                note?.title=self.title
                                note?.updateTimeStamp=Date().milliStamp
                                update(note: self.note!)
                            }
                            
                        }, label: {
                            Text("保存").frame(width: 100, height: 30, alignment: .center).padding(.vertical,5).padding(.horizontal,20)
                            
                        }).background(Color.color_golden_text).foregroundColor(.white).cornerRadius(20)
                        .padding(.top,50)
                        
                        Spacer()
                    }
                    Spacer()
                }
            }.navigationBarHidden(true)
        }.onAppear(perform: {
            self.title=self.note?.title ?? ""
            self.content=self.note?.content ?? ""
        }).navigationTitle(gettitle(title: self.title))
        .navigationBarTitle(gettitle(title: self.title))
        .navigationBarBackButtonHidden(true)
        .navigationBarItems(leading: Text("返回").onTapGesture {
            presentationMode.wrappedValue.dismiss()
        })
        .navigationBarTitleDisplayMode(.inline)
    }
    func  insert() -> Void {
        let note:NoteModel = NoteModel(title: title, content: content, createTimeStamp: createTime, updateTimeStamp: createTime)
        let success = sqliteViewModel.insert(note: note)
        if(success){
            // 成功插入后查询
            Toast(text: "保存成功").show()
            presentationMode.wrappedValue.dismiss()
        }
    }
    
    func update(note:NoteModel) -> Void {
        let success = sqliteViewModel.update(note: note)
        if(success){
            // 成功更新后查询
            Toast(text: "更新成功").show()
            presentationMode.wrappedValue.dismiss()
        }else{
            Toast(text: "更新失败").show()
        }
    }
    func gettitle(title:String) -> String {
        return title.isEmpty ? "创建":"编辑"
    }
}


struct WriteNote_Previews: PreviewProvider {
    static var previews: some View {
        WriteNote()
    }
}
