//
//  noteswiftApp.swift
//  noteswift
//
//  Created by lzh on 2022/5/31.
//

import SwiftUI

@main
struct noteswiftApp: App {
    @Environment(\.scenePhase) var scenePhase
    var body: some Scene {
        WindowGroup {
            MainView()
        }.onChange(of: scenePhase){
            (newScenePhase) in switch newScenePhase{
            case .active:
                print("active==处于前台")
                break
            case .background:
                print("background==处于后台")
            case .inactive:
                print("inactive==不活动")
                break
                        default:
                break
            }
        }
    }
}
