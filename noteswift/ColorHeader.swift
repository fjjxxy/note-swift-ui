//
//  ColorHeader.swift
//  noteswift
//
//  Created by lzh on 2022/5/31.
//
import Foundation
import SwiftUI

extension Color{
    public static var color_gray_border: Color {
        Color("color_gray_border", bundle: nil)
    }
    public static var color_golden_text: Color {
        Color("color_golden_text", bundle: nil)
    }
}
