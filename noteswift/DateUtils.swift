//
//  DateUtils.swift
//  noteswift
//
//  Created by lzh on 2022/6/1.
//
import UIKit

extension Date {
    /// 获取当前 秒级 时间戳 - 10位
    var timeStamp : String {
        let timeInterval: TimeInterval = self.timeIntervalSince1970
        let timeStamp = Int(timeInterval)
        return "\(timeStamp)"
    }
    /// 获取当前 毫秒级 时间戳 - 13位
    var milliStamp : String {
        let timeInterval: TimeInterval = self.timeIntervalSince1970
        let millisecond = CLongLong(round(timeInterval*1000))
        return "\(millisecond)"
    }
    
   static func  stampToString(stamp:String) -> String{
        let timeInterval: TimeInterval = TimeInterval(Double(stamp) ?? 0)
    
        let date=Date.init(timeIntervalSince1970: round(timeInterval/1000))
    print("\(timeInterval);\(date)")
        let dateFormat=DateFormatter()
        dateFormat.dateFormat="yyyy-MM-dd HH:mm:ss"
        return dateFormat.string(from: date)
    }
}
