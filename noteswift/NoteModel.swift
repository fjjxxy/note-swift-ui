//
//  NoteModel.swift
//  
//
//  Created by lzh on 2022/5/31.
//

import Foundation

struct NoteModel: Identifiable{
    var id = UUID()
    var title : String=""
    var content : String=""
    var createTimeStamp:String="0"
    var updateTimeStamp:String="0"
    
}
